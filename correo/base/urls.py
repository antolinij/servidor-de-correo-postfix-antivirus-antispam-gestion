from django.conf.urls import patterns, url
from base import views

urlpatterns= patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^home/$', views.home, name='home'),
	url(r'^login/$', views.user_login, name='login'),
        url(r'^logout/$', views.user_logout, name='logout'),
	url(r'^mailbox/$', views.mailbox, name='mailbox'),
	url(r'^domain/$', views.domain, name='domain'),


	#url(r'^activar/new/$', views.activar_doble_factor, name='activar'),
	#url(r'^activar/persist/$', views.activar_doble_factor, name='activar'),
	)
