from django.db import models

class Aliases(models.Model):
    pkid = models.AutoField(primary_key=True)
    mail = models.CharField(unique=True, max_length=120)
    destination = models.CharField(max_length=120)
    enabled = models.IntegerField(default=1)
    class Meta:
        db_table = 'aliases'

class Domains(models.Model):
    pkid = models.AutoField(primary_key=True)
    domain = models.CharField(max_length=120)
    transport = models.CharField(max_length=120, default='virtual:')
    enabled = models.IntegerField(default=1)
    class Meta:
        db_table = 'domains'

class Users(models.Model):
    id = models.CharField(primary_key= True, unique=True, max_length=128)
    name = models.CharField(max_length=128)
    uid = models.IntegerField(default=5000)
    gid = models.IntegerField(default=5000)
    home = models.CharField(max_length=255, default='/var/spool/mail/virtual')
    maildir = models.CharField(max_length=255, default='blah/')
    enabled = models.IntegerField(default=1)
    change_password = models.IntegerField(default=1)
    crypt = models.CharField(max_length=128, default='sdtrusfX0Jj66')
    clear = models.CharField(max_length=128, default='ChangeMe')
    quota = models.CharField(max_length=255, default='')
    
    class Meta:
	db_table = 'users'
