from django import forms
from django.contrib.auth.models import User
from base.models import Users

class UserForm(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())

	class Meta:
		model = User
		fields = ('username', 'email', 'password')


class MailForm(forms.ModelForm):
        crypt = forms.CharField(widget=forms.PasswordInput())

        class Meta:
                model = Users
                fields = ('name', 'crypt')

