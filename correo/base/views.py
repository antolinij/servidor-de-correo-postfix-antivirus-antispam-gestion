from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from base.forms import UserForm, MailForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from base.models import Users, Domains, Aliases
from django.core.mail import send_mail
import random, crypt


def index(request):   
    #creamos un diccionario para pasar al template
    context_dict = {'variable': "Vengo de la vista y soy una variable"}
    if request.user.is_authenticated():
	#return render(request, 'base/home.html')
	return HttpResponseRedirect('/home/')    

    return render(request, 'base/index.html', context_dict)

@login_required(login_url='/')
def home(request):   
	return render(request, 'base/home.html', {'security': 'actived'})

def user_login(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/home/')
	if request.method == 'POST':
        	#capturamos los datos del formulario de inicio de sesion
        	username = request.POST['username']
        	password = request.POST['password']
	
		user = authenticate(username=username, password=password)

		if user:
    			if user.is_active:
                		login(request, user)
                		return HttpResponseRedirect('/home/')
           	 	else:
                		return HttpResponse("Tu usuario esta desactivado")
        	else:
            		return render(request, 'base/index.html', {'error_login': 'Hubo un error en el login'})
	else:
        	if not request.user.is_authenticated():
           		return render(request, 'base/index.html', {})
        	else:
            		return HttpResponseRedirect('/home/') 

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/')

@login_required
def mailbox(request):

	domain_list= Domains.objects.all()
	registered= False
	if request.method == 'POST':
		req_dict= request.POST.dict()
		if req_dict['list'] == None:
			return render(request, 'base/mailbox.html', {'error_msg': 'Debe seleccionar un dominio', 'domain_list':domain_list})
		mail_form = MailForm(data=request.POST)
		mail= mail_form.save(commit=False)
		if mail_form.is_valid():
			try:
				name_valid= Users.objects.get(name=mail.name)
				return render(request, 'base/mailbox.html', {'error_msg': mail.name, 'domain_list': domain_list})
			except:
				pass
			salt= "$5$"+str(random.random())
			crypt_form= crypt.crypt(mail.crypt, salt)
			maildir_form= str(mail.name)+str("/")
			id_form= str(mail.name)+str('@')+str(req_dict['list'])
			name_form= str(mail.name)

			u= Users(id= id_form, name=name_form, maildir=maildir_form, crypt=crypt_form)
			a= Aliases(mail= id_form, destination= id_form)
			a.save()
			u.save()
			send_mail('Bienvenido al correo Ardilla', 'No responda este correo', 'no-reply@ardilla.com.ar', [id_form], fail_silently=False)

			registered = True
			return render(request, 'base/home.html', {'register_msg': 'Registro satisfactorio, ya puede usar su usuario en http://ardilla.com.ar/webmail/'} )
		else:
			return render(request, 'base/mailbox.html', {'error_msg': 'Error en campos ', 'domain_list': domain_list})
	else:
		#devolvemos un formulario al template
		mail_form = MailForm()
		return render(request, 'base/mailbox.html', {'user_form': mail_form, 'domain_list': domain_list})

@login_required
def domain(request):

	domain_list= Domains.objects.all()
	if request.method == 'POST':
		req_dict= request.POST.dict()
		if req_dict['domain'] == None:
			return render(request, 'base/domain.html', {'error_msg': 'Debe ingresar un dominio', 'domain_list':domain_list})
		try:
			domain_valid= Domains.objects.get(domain=req_dict['domain'])
			return render(request, 'base/domain.html', {'error_msg': 'El dominio ya existe', 'domain_list': domain_list})
		except:
			pass

		d= Domains(domain= req_dict['domain'])
		alias= '@'+str(req_dict['domain'])
			#set antolinij@ardilla.com.ar como el super usuario en caso de que no exista un usuario con el dominio nuevo.
		a= Aliases(mail= alias, destination= 'antolinij@ardilla.com.ar')
		a.save()
		d.save()

		return render(request, 'base/home.html', {'domain_msg': 'Dominio agregado satisfactoriamente'} )
	else:
		#devolvemos un formulario al template
		return render(request, 'base/domain.html', {'domain_list': domain_list})

