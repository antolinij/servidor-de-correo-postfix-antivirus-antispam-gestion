-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: maildb
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.10.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aliases`
--

DROP TABLE IF EXISTS `aliases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aliases` (
  `pkid` smallint(3) NOT NULL AUTO_INCREMENT,
  `mail` varchar(120) NOT NULL DEFAULT '',
  `destination` varchar(120) NOT NULL DEFAULT '',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pkid`),
  UNIQUE KEY `mail` (`mail`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aliases`
--

LOCK TABLES `aliases` WRITE;
/*!40000 ALTER TABLE `aliases` DISABLE KEYS */;
INSERT INTO `aliases` VALUES (1,'postmaster@localhost','root@localhost',1),(2,'sysadmin@localhost','root@localhost',1),(3,'webmaster@localhost','root@localhost',1),(4,'abuse@localhost','root@localhost',1),(5,'root@localhost','root@localhost',1),(6,'@localhost','root@localhost',1),(7,'@localhost.localdomain','@localhost',1),(8,'postmaster@ardilla.com.ar','postmaster@localhost',1),(9,'abuse@ardilla.com.ar','abuse@localhost',1);
/*!40000 ALTER TABLE `aliases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `domains`
--

DROP TABLE IF EXISTS `domains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `domains` (
  `pkid` smallint(6) NOT NULL AUTO_INCREMENT,
  `domain` varchar(120) NOT NULL DEFAULT '',
  `transport` varchar(120) NOT NULL DEFAULT 'virtual:',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pkid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `domains`
--

LOCK TABLES `domains` WRITE;
/*!40000 ALTER TABLE `domains` DISABLE KEYS */;
INSERT INTO `domains` VALUES (1,'localhost','virtual:',1),(2,'localhost.localdomain','virtual:',1),(3,'ardilla.com.ar','virtual:',1),(4,'mail.ardilla.com.ar','virtual:',1);
/*!40000 ALTER TABLE `domains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` varchar(128) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT '',
  `uid` smallint(5) unsigned NOT NULL DEFAULT '5000',
  `gid` smallint(5) unsigned NOT NULL DEFAULT '5000',
  `home` varchar(255) NOT NULL DEFAULT '/var/spool/mail/virtual',
  `maildir` varchar(255) NOT NULL DEFAULT 'blah/',
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `change_password` tinyint(1) NOT NULL DEFAULT '1',
  `clear` varchar(128) NOT NULL DEFAULT 'ChangeMe',
  `crypt` varchar(128) NOT NULL DEFAULT 'sdtrusfX0Jj66',
  `quota` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('antolinij@ardilla.com.ar','antolinij',5000,5000,'/var/spool/mail/virtual','antolinij/',1,1,'ChangeMe','$5$42f40465e9921682$lOqWK.wpp.l/YO1O1UrRpeZqIaUTB6M1ds.s.R/E7h2',''),('root@localhost','root',5000,5000,'/var/spool/mail/virtual','root/',1,1,'ChangeMe','$5$483575b5f3c2f3f8$n/IOoe91Y252f7WjxNKo9KQlwQYC3Q99D/3pmMlkVo.',''),('usuario@ardilla.com.ar','usuario',5000,5000,'/var/spool/mail/virtual','usuario/',1,1,'ChangeMe','$5$c6443eb61daa859c$iKWIdPRCyjoRe1SUyl/1AK/LA3gO1uE1GlFVjjvwvi9','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-03-25 15:17:30
